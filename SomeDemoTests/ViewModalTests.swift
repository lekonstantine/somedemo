//
//  SomeDemoTests.swift
//  SomeDemoTests
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import XCTest
@testable import SomeDemo

class SomeDemoTests: XCTestCase {

    var delivery: Delivery? = nil
    var viewModal: DeliveryModalView? = nil
    
    override func setUpWithError() throws {
        
        delivery = Delivery()
        
        let start = "Central"
        let end = "Kennedy Town"
        let route = Route(start: start, end: end)
        delivery?.route = route
        
        delivery?.id = "someID"
        delivery?.deliveryFee = "$105.5"
        delivery?.surcharge = "$25.5"
        delivery?.goodsPicture = "https://static.toiimg.com/photo/msid-67586673/67586673.jpg"
        
        viewModal = DeliveryModalView(delivery)
    }
    
    func testFromLabel() throws {
    
        let givenString = viewModal!.getFromLabelText()
        let start = delivery!.route!.start!
        let expectedString = "From: \(start)"
        
        XCTAssertNotNil(givenString)
        XCTAssertEqual(givenString, expectedString)
    }
    
    func testToLabel() throws {
    
        let givenString = viewModal!.getToLabelText()
        let start = delivery!.route!.end!
        let expectedString = "To: \(start)"
        
        XCTAssertNotNil(givenString)
        XCTAssertEqual(givenString, expectedString)
    }
    
    func testFeeLabel() throws {
    
        let givenString = viewModal!.getFeeLabelText()
        let expectedString = "$131"
        
        XCTAssertNotNil(givenString)
        XCTAssertEqual(givenString, expectedString)
    }
    
    func testPriceLabel() throws {
    
        let givenString = viewModal!.getPriceLabelText()
        let expectedString = "Price: $131"
        
        XCTAssertNotNil(givenString)
        XCTAssertEqual(givenString, expectedString)
    }

    func testImageURLExist() {
        XCTAssertNotNil(viewModal?.getImagePath())
    }
    
    func testIDMatch() {
        let id = viewModal?.getID()
        XCTAssertNotNil(id)
        XCTAssertEqual(id!, "someID")
    }
}
