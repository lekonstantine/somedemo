//
//  LocalStorageTests.swift
//  SomeDemoTests
//
//  Created by Konstantin Skoblik on 6/22/20.
//  Copyright © 2020 Home. All rights reserved.
//

import XCTest
@testable import SomeDemo

class LocalStorageTests: XCTestCase {

    let storage = LocalStorage.shared
    
    override func setUpWithError() throws {
        storage.dataBaseName = "testDB"
        storage.favoriteDeliveries.removeAll()
    }
    
    func testAdd() {
        storage.add("id1")
        storage.add("id1")
        XCTAssertEqual(storage.favoriteDeliveries.count, 1)
    }

    func testRemove() {
        storage.add("id1")
        storage.remove("id1")
        XCTAssertEqual(storage.favoriteDeliveries.count, 0)
    }
    
    func testFavorite() {
        let id = "id1"
        storage.add(id)
        XCTAssertTrue(storage.isFavorite(id))
    }
}
