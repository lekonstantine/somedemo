//
//  DeliveryProvider.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import Alamofire

class DeliveryProvider: NSObject {

    func loadDeliveries(_ skip: Int, closure: @escaping ([Delivery])->()) {
        
        let path = "https://mock-api-mobile.dev.lalamove.com/v2/deliveries?offset=\(skip)&limit=20"
        
        AF.request(path).response { [weak self] response in
            let data = response.data
            let items = self?.parseResponse(data) ?? [Delivery]()
            closure(items)
        }
    }
    
    func parseResponse(_ data: Data?) -> [Delivery] {
        
        var result = [Delivery]()
        
        if let data = data {
            
            do {
                if let array = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any?]] {
                    
                    for item in array {
                        let delivery = Delivery(data: item)
                        result.append(delivery)
                    }
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        return result
    }
}
