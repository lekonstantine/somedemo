//
//  UIFactory.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

class UIFactory: NSObject {

    class func getLabel(_ fontSize: CGFloat = 16, fontWeight: UIFont.Weight = .regular) -> UILabel {
    
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        
        return label
    }
}
