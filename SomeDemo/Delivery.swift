//
//  Delivery.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

struct Route {
    var start: String?
    var end: String?
}

struct Sender {
    var phone: String?
    var name: String?
    var email: String?
}

class Delivery: NSObject {
    
    var id: String?
    var remarks: String?
    var pickupTime: String?
    var goodsPicture: String?
    var deliveryFee: String?
    var surcharge: String?
    var route: Route?
    var sender: Sender?
    
    override init() {
        super.init()
        
    }
    
    init(data: [String: Any?]) {
        super.init()
        
        id = data["id"] as? String
        remarks = data["remarks"] as? String
        pickupTime = data["pickupTime"] as? String
        goodsPicture = data["goodsPicture"] as? String
        deliveryFee = data["deliveryFee"] as? String
        surcharge = data["surcharge"] as? String
        
        if let routeData = data["route"] as? [String: Any?] {
            route = Route(
                start: routeData["start"] as? String,
                end: routeData["end"] as? String
            )
        }
        
        if let senderData = data["sender"] as? [String: Any?] {
            sender = Sender(
                phone: senderData["phone"] as? String,
                name: senderData["name"] as? String,
                email: senderData["email"] as? String
            )
        }
    }
    
    func getPrice() -> Int {
        
        guard let deliveryFee = deliveryFee, let surcharge = surcharge else { return 0 }
        
        let fee = Double(deliveryFee.replacingOccurrences(of: "$", with: "")) ?? 0
        let charge = Double(surcharge.replacingOccurrences(of: "$", with: "")) ?? 0
        
        return Int(fee + charge)
    }
}
