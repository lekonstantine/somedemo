//
//  DeliveryCell.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import AlamofireImage

class DeliveryCell: UITableViewCell {
    
    var viewModal: DeliveryModalView?
    
    static let defaultHeight = CGFloat(80)
    
    var goodPictureImageView = UIImageView()
    var labelFrom = UILabel()
    var labelTo = UILabel()
    var labelPrice = UILabel()
    var labelRemarks = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.clipsToBounds = true

        addGoodImageView()
        addLabelFrom()
        addLabelTo()
        addLabelPrice()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(listenFavoriteEvent),
            name: .deliveryStatusChanged, object: nil
        )
    }
    
    @objc func listenFavoriteEvent() {
        updateFavoriteStatus()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        labelTo.text = nil
        labelFrom.text = nil
        goodPictureImageView.image = nil
        backgroundColor = .white
    }
    
    func configure(_ viewModal: DeliveryModalView?) {
        
        guard let vm = viewModal else { return }
        
        self.viewModal = vm
        
        labelFrom.text = vm.getFromLabelText()
        labelTo.text = vm.getToLabelText()
        labelPrice.text = vm.getFeeLabelText()
        
        if let path = vm.getImagePath(), let url = URL(string: path) {
            goodPictureImageView.af.setImage(withURL: url)
        }
        
        updateFavoriteStatus()
    }
    
    func updateFavoriteStatus() {
        
        let isFavorite = LocalStorage.shared.isFavorite(viewModal?.getID())
        
        if isFavorite {
            backgroundColor = .yellow
        } else {
            backgroundColor = .white
        }
    }
    
    func addGoodImageView() {
        
        goodPictureImageView = UIImageView()
        goodPictureImageView.contentMode = .scaleAspectFit
        goodPictureImageView.translatesAutoresizingMaskIntoConstraints = false
        goodPictureImageView.clipsToBounds = true
        contentView.addSubview(goodPictureImageView)
        
        goodPictureImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        goodPictureImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        goodPictureImageView.widthAnchor.constraint(equalToConstant: 62).isActive = true
        goodPictureImageView.heightAnchor.constraint(equalToConstant: 62).isActive = true
    }
    
    func addLabelFrom() {
        
        labelFrom = UIFactory.getLabel()
        contentView.addSubview(labelFrom)
        
        labelFrom.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        labelFrom.leadingAnchor.constraint(equalTo: goodPictureImageView.trailingAnchor, constant: 16).isActive = true
        labelFrom.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -80).isActive = true
    }
    
    func addLabelTo() {
        
        labelTo = UIFactory.getLabel()
        contentView.addSubview(labelTo)
        
        labelTo.topAnchor.constraint(equalTo: labelFrom.bottomAnchor, constant: 8).isActive = true
        labelTo.leadingAnchor.constraint(equalTo: goodPictureImageView.trailingAnchor, constant: 16).isActive = true
        labelTo.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -80).isActive = true
    }
    
    func addLabelPrice() {
        
        labelPrice = UIFactory.getLabel(20, fontWeight: .medium)
        contentView.addSubview(labelPrice)
        
        labelPrice.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30).isActive = true
        labelPrice.leadingAnchor.constraint(equalTo: labelTo.trailingAnchor, constant: 16).isActive = true
        labelPrice.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
    }
}
