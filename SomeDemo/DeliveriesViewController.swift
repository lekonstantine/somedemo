//
//  DeliveriesViewController.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

class DeliveriesViewController: UIViewController {

    let provider = DeliveryProvider()
    var deliveries = [Delivery]()
    var labelNoElements = UILabel()
    var tableView: UITableView?
    let refreshControl = UIRefreshControl()
    var indicator = UIActivityIndicatorView(style: .medium)
    let footerIndicator = UIActivityIndicatorView(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Deliveries"
        
        initTableView()
        addActivityIndicator()
        addLabelNoElements()
        
        loadData(0, isRefresh: true)
    }
    
    private func initTableView() {
        
        let tableView = UITableView(frame: UIScreen.main.bounds, style: UITableView.Style.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DeliveryCell.self, forCellReuseIdentifier: "DeliveryCell")
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 90, bottom: 0, right: 0)
        
        self.tableView = tableView
        self.view.addSubview(tableView)
        self.view.backgroundColor = .white
        
        refreshControl.addTarget(self, action:  #selector(refreshTable), for: .valueChanged)
        self.tableView?.refreshControl = refreshControl
    }
    
    func addActivityIndicator() {
        
        self.view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        indicator.hidesWhenStopped = true
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        let footerView = UIView()
        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        footerView.addSubview(footerIndicator)
        footerIndicator.translatesAutoresizingMaskIntoConstraints = false
        footerIndicator.centerXAnchor.constraint(equalTo: footerView.centerXAnchor).isActive = true
        footerIndicator.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
        footerIndicator.hidesWhenStopped = true
        tableView?.tableFooterView = footerView
    }
    
    func addLabelNoElements() {
        
        labelNoElements = UIFactory.getLabel()
        labelNoElements.text = "No elements\nPlease pull to refresh"
        labelNoElements.textAlignment = .center
        labelNoElements.numberOfLines = 2
        labelNoElements.isHidden = true
        self.view.addSubview(labelNoElements)
        self.view.bringSubviewToFront(labelNoElements)
        
        labelNoElements.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        labelNoElements.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        labelNoElements.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32).isActive = true
        labelNoElements.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
    }
    
    func loadData(_ skip: Int = 0, isRefresh: Bool = false) {
        
        if isRefresh {
            indicator.startAnimating()
            tableView?.isHidden = true
            footerIndicator.stopAnimating()
        } else {
            footerIndicator.startAnimating()
        }
        
        provider.loadDeliveries(skip) { [weak self] (items) in
            
            if isRefresh {
                self?.deliveries = items
            } else {
                self?.deliveries += items
            }
            
            self?.indicator.stopAnimating()
            self?.tableView?.reloadData()
            self?.tableView?.isHidden = false
            self?.refreshControl.endRefreshing()
            
            self?.showEmptyLabel()
        }
    }
    
    @objc func refreshTable() {
        loadData(0, isRefresh: true)
    }
    
    func showEmptyLabel() {
        
        if deliveries.count == 0 {
            labelNoElements.isHidden = false
        } else {
            labelNoElements.isHidden = true
        }
    }
}



extension DeliveriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryCell", for: indexPath) as! DeliveryCell

        let delivery = deliveries[indexPath.row]
        let viewModal = DeliveryModalView(delivery)
        
        cell.configure(viewModal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeliveryCell.defaultHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = DeliveryDetailsViewController()
        vc.delivery = deliveries[indexPath.row]
        self.show(vc, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let count = deliveries.count
        
        if indexPath.row >= count - 1 {
            loadData(count)
        }
    }
}
