//
//  DeliveryDetailsViewController.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import AlamofireImage

class DeliveryDetailsViewController: UIViewController {

    var delivery: Delivery?
    
    var goodPictureImageView = UIImageView()
    var labelFrom = UILabel()
    var labelTo = UILabel()
    var labelPrice = UILabel()
    var labelRemarks = UILabel()
    var buttonFavorite = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Details"
        self.view.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addGoodImageView()
        addLabelFrom()
        addLabelTo()
        addLabelPrice()
        addLabelRemarks()
        addButtonFavorite()
        
        setData()
        
        updateFavoriteSatus()
    }
    
    func setData() {
        
        let vm = DeliveryModalView(delivery)
        
        labelFrom.text = vm.getFromLabelText()
        labelTo.text = vm.getToLabelText()
        labelPrice.text = vm.getPriceLabelText()
        labelRemarks.text = delivery?.remarks
        
        if let path = vm.getImagePath(), let url = URL(string: path) {
            goodPictureImageView.af.setImage(withURL: url)
        }
    }
    
    func addGoodImageView() {
        
        goodPictureImageView = UIImageView()
        goodPictureImageView.contentMode = .scaleAspectFit
        goodPictureImageView.translatesAutoresizingMaskIntoConstraints = false
        goodPictureImageView.clipsToBounds = true
        self.view.addSubview(goodPictureImageView)
        
        goodPictureImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        goodPictureImageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100).isActive = true
        goodPictureImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        goodPictureImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    }
    
    func addLabelFrom() {
        
        labelFrom = UIFactory.getLabel()
        self.view.addSubview(labelFrom)
        
        labelFrom.topAnchor.constraint(equalTo: goodPictureImageView.bottomAnchor, constant: 18).isActive = true
        labelFrom.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        labelFrom.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    }
    
    func addLabelTo() {
        
        labelTo = UIFactory.getLabel()
        self.view.addSubview(labelTo)
        
        labelTo.topAnchor.constraint(equalTo: labelFrom.bottomAnchor, constant: 8).isActive = true
        labelTo.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        labelTo.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    }
    
    func addLabelPrice() {
        
        labelPrice = UIFactory.getLabel()
        self.view.addSubview(labelPrice)
        
        labelPrice.topAnchor.constraint(equalTo: labelTo.bottomAnchor, constant: 16).isActive = true
        labelPrice.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        labelPrice.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    }
    
    func addLabelRemarks() {
        
        labelRemarks = UIFactory.getLabel()
        labelRemarks.numberOfLines = 3
        self.view.addSubview(labelRemarks)
        
        labelRemarks.topAnchor.constraint(equalTo: labelPrice.bottomAnchor, constant: 16).isActive = true
        labelRemarks.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        labelRemarks.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
    }
    
    func addButtonFavorite() {
        
        buttonFavorite.clipsToBounds = true
        buttonFavorite.layer.cornerRadius = 8
        buttonFavorite.backgroundColor = .red
        buttonFavorite.setTitle("Add to Favorite", for: .normal)
        buttonFavorite.translatesAutoresizingMaskIntoConstraints = false
        buttonFavorite.addTarget(self, action: #selector(clickOnFavoriteButton), for: .touchUpInside)
        self.view.addSubview(buttonFavorite)
        
        buttonFavorite.heightAnchor.constraint(equalToConstant: 44).isActive = true
        buttonFavorite.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -48).isActive = true
        buttonFavorite.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32).isActive = true
        buttonFavorite.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
    }
    
    @objc func clickOnFavoriteButton() {
        
        let isFavorite = LocalStorage.shared.isFavorite(delivery?.id)
        
        if isFavorite {
            LocalStorage.shared.remove(delivery?.id)
        } else {
            LocalStorage.shared.add(delivery?.id)
        }
        
        updateFavoriteSatus()
    }
    
    func updateFavoriteSatus() {
        
        var color: UIColor = .green
        var title = "Add to Favorite"
        let isFavorite = LocalStorage.shared.isFavorite(delivery?.id)
        
        if isFavorite {
            color = .red
            title = "Remove from Favorite"
        }
        
        buttonFavorite.setTitle(title, for: .normal)
        buttonFavorite.backgroundColor = color
    }
}

