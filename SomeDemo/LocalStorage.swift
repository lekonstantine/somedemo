//
//  LocalStorage.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

class LocalStorage: NSObject {

    static let shared = LocalStorage()
    var dataBaseName = "default"
    
    var favoriteDeliveries = Set<String>()
    
    private override init() {
        super.init()
        
        load()
    }
    
    func add(_ id: String?) {
        
        guard let id = id else { return }
        
        favoriteDeliveries.insert(id)
        save()
        
        NotificationCenter.default.post(name: .deliveryStatusChanged, object: id)
    }
    
    func remove(_ id: String?) {
        
        guard let id = id else { return }
        
        favoriteDeliveries.remove(id)
        save()
        
        NotificationCenter.default.post(name: .deliveryStatusChanged, object: id)
    }
    
    func isFavorite(_ id: String?) -> Bool {
        guard let id = id else { return false }
        return favoriteDeliveries.contains(id)
    }
    
    
    func save() {
        
        let UD = UserDefaults.standard
        UD.set(Array(favoriteDeliveries), forKey: dataBaseName)
        UD.synchronize()
    }
    
    func load() {
        
        let UD = UserDefaults.standard
        if let value = UD.object(forKey: dataBaseName) as? [String] {
            favoriteDeliveries = Set(value)
        }
    }
}


extension Notification.Name {
    static let deliveryStatusChanged = Notification.Name("DeliveryStatusChanged")
}
