//
//  DeliveryModalView.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/22/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

class DeliveryModalView: NSObject {

    private var delivery: Delivery?
    
    override init() {
        super.init()
    }
    
    init(_ delivery: Delivery?) {
        super.init()
        
        self.delivery = delivery
    }
    
    func getID() -> String? {
        return delivery?.id
    }
    
    func getFromLabelText() -> String {
        return "From: \(delivery?.route?.start ?? "")"
    }
    
    func getToLabelText() -> String {
        return "To: \(delivery?.route?.end ?? "")"
    }
    
    func getFeeLabelText() -> String {
        return "$\(delivery?.getPrice() ?? 0)"
    }
    
    func getPriceLabelText() -> String {
        return "Price: $\(delivery?.getPrice() ?? 0)"
    }
    
    func getImagePath() -> String? {
        return delivery?.goodsPicture
    }
}
