//
//  SceneDelegate.swift
//  SomeDemo
//
//  Created by Konstantin Skoblik on 6/21/20.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let _ = (scene as? UIWindowScene) else { return }
        
        let deliveriesVC = DeliveriesViewController()
        window?.rootViewController = UINavigationController(rootViewController: deliveriesVC)
        window?.makeKeyAndVisible()
    }
}

